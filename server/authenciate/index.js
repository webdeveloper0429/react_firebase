var firebase = require("firebase");

module.exports.isAuthenticated = function(redirect){
    return function(req, res, next){
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                // User is signed in.
                var displayName = user.displayName;
                var email = user.email;
                var emailVerified = user.emailVerified;
                var photoURL = user.photoURL;
                var isAnonymous = user.isAnonymous;
                var uid = user.uid;
                var providerData = user.providerData;
                
                return next();
            } else {
                if(redirect){
                    return res.redirect(redirect);
                } else {
                    return res.status(401).send();
                }
            }
        })
    }
}