var express = require('express');
var router = express.Router();
var UserService = require('../service/UserService');

router.get('/', function(req, res){
    UserService.find(req.query).then(function(data){
        res.status(200).send(data)
    }).catch(function(err){
        res.status(404).send(err);
    });
});

router.post('/', function(req, res){
    console.log(req.body)
    UserService.createUser(req.body).then(function(data){
        res.status(200).send(data)
    }).catch(function(err){
        res.status(404).send(err);
    });
});

module.exports = router;