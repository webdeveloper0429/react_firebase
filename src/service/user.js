import axios from 'axios';

export class UserService {

    static BASE_URL = 'api/users/';
    
    static find(query){
        return axios({
            method: 'get',
            url: '',
            params: query,
            baseURL: this.BASE_URL
        });
    }

    static createUser(data){
        return axios({
            method: 'post',
            url: '',
            data: data,
            baseURL: this.BASE_URL
        });
    }
}