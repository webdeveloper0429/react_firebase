import React from 'react'
import { Route, IndexRoute } from 'react-router';
import {Login} from '../components/Login/Login';
import {Hello} from '../components/Hello/Hello';
import {Signup} from '../components/Login/Signup';
import {AdminBoard} from '../components/AdminBoard/AdminBoard'


export default (store) => {
    return (
        <Route path="" >
            <Route path='login' component={Login} />
            <Route path='signup' component={Signup} />
            <Route path='admin' component={AdminBoard} />
            <Route path="*" component={Hello} />
        </Route>
    )
}
