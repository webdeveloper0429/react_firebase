import React from "react";
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Textarea from 'muicss/lib/react/textarea';
import Button from 'muicss/lib/react/button';
import Panel from 'muicss/lib/react/panel';

export class Login extends React.Component{
	constructor(props){
		super(props);
	}
	render(){
		return(
			<div className="loginForm">
				<Panel className="formPanel">
					<Form>
						<legend>LogIn</legend>
						<Input placeholder="Username" />
						<Input placeholder="Password" type="password"/>
						<Button variant="raised" color="primary">button</Button>
					</Form>
				</Panel>
			</div>
		)
	}
}