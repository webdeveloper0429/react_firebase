import React from "react";
import {UserService} from '../../service/user';

import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Textarea from 'muicss/lib/react/textarea';
import Button from 'muicss/lib/react/button';
import Panel from 'muicss/lib/react/panel';

export class Signup extends React.Component{
	constructor(props){
		super(props);
		this.state={
			username: '',
			password: '',
			email: '',
			firstName: '',
			lastName: '',
			company: ''
		}
	}
	onSubmit = (e)=>{
		e.preventDefault();
		let newUserData = {
			username: this.state.username,
			password: this.state.password,
			email: this.state.email,
			firstName: this.state.firstName,
			lastName: this.state.lastName,
			company: this.state.company
		}
		UserService.createUser(newUserData).then(
			result=>{
				debugger;
			}
		)
	}
	render(){
		return(
			<div className="loginForm">
				<Panel className="formPanel">
					<Form>
						<legend>Sign Up</legend>
						<Input 
							placeholder="Username"
							value={this.state.username}
							onChange={ (e)=>{ this.setState({username: e.target.value}) } }
						/>
						<Input 
							placeholder="Password"
							type="password"
							value={this.state.password}
							onChange={ (e)=>{ this.setState({password: e.target.value}) } }
						/>
						<Input 
							placeholder="First Name"
							value={this.state.firstName}
							onChange={ (e)=>{ this.setState({firstName: e.target.value}) } }
						/>
						<Input 
							placeholder="Lase Name"
							value={this.state.lastName}
							onChange={ (e)=>{ this.setState({lastName: e.target.value}) } }
						/>
						<Input 
							placeholder="Email"
							value={this.state.email}
							onChange={ (e)=>{ this.setState({email: e.target.value}) } }
						/>
						<Input 
							placeholder="Company"
							value={this.state.company}
							onChange={ (e)=>{ this.setState({company: e.target.value}) } }
						/>
						<Button variant="raised" color="primary" onClick={this.onSubmit}>Submit</Button>
					</Form>
				</Panel>
			</div>
		)
	}
}