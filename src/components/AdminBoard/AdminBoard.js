import React from "react";
import {UserService} from '../../service/user';

import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Button from 'muicss/lib/react/button';
import Panel from 'muicss/lib/react/panel';

export class AdminBoard extends React.Component{
	constructor(props){
        super(props);
        this.state={
            limit: 100,
            userData: [],
        }
        this.loadDataFromServer();
    }
    loadDataFromServer = ()=>{
        let query = {
            limit: this.state.limit
        }
        UserService.find(query).then(
            resp=>{
                console.log(resp)
            }
        )
    }
	render(){
		return(
			<div className="container">
				<table style={{width: '100%'}}>
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>FirstName</th>
                            <th>LastName</th>
                            <th>Email</th>
                            <th>Company</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
			</div>
		)
	}
}